date: "2021-07-20T15:51:07Z"
slug: if-only-abstract-art-edition
tags: notlikely
title: If only... Abstract Art edition
---
In 8th grade my Art teacher recommended I pursue (somewhat disdainfully I suspect) "commercial art" (graphic design, in the 70s) because I loved straight lines and geometric shapes. If only she'd studied Hilda af Klint, Paul Klee, and other abstract artists, I might be an artist right now instead of a computer nerd 🤣 #notlikely

![http://monkinetic.blog/uploads/Screen Shot 2021-07-20 at 8.50.41 AM.png](http://monkinetic.blog/uploads/Screen Shot 2021-07-20 at 8.50.41 AM.png)
