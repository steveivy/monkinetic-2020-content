date: "2024-04-10T13:33:01Z"
slug: disneys-lost-greeenscreen-tech-re-discovered
tags: movies,specialeffects
title: Disney's lost "greenscreen" tech re-discovered
---
In the 1960s, Disney discovered and used an incredible "greenscreen" technique to film the Mary Poppins actors in an animated scene, perfectly capturing flowing semi-transparent clothes and motion blur and sharp animated characters. They then lost the tech.

[Corridor Crew](https://www.youtube.com/channel/UCSpFnDQr88xCZ80N-X7t0nQ) found out about it, someone figured out how to replicate it, and they worked to demonstrate it. And it's fucking amazing.

https://m.youtube.com/watch?v=UQuIVsNzqDk
