date: "2024-04-11T18:39:03Z"
slug: a-bit-of-blog-complaining
tags: blogging,goldfrog,userlandfrontier
title: A Bit Of Blog Complaining
---
Guess it's time to revisit the blog engine here. I wrote [Goldfrog](https://gitlab.com/steveivy/goldfrog) a few years ago and it's been chugging along on this [Digital Ocean](https://www.digitalocean.com/) instance fairly well, but at the time I had in mind a two-way sync between gitlab, where I maintain a separate repository of my archived content, and the filesystem/db in Goldfrog.

It worked, sort of, for a while, but the deployment on DO is NOT simple to remember, uses Ansible and code from 2 different git repos to set up or update the server, and was just 3 times more clever than it should have been.

I also implemented a flexible/configurable [POSSE](https://indieweb.org/POSSE) feature that is supposed to send updates to my [mastodon account](https://hachyderm.io/@sivy/) but ... isn't right now? And the logging setup on the site is abysmal.

I still like parts of my system. If I did it again, I'd still want:

- My custom posting UI that works like the ancient Radio Userland sites did: post form at the top of the home page list of posts:

![https://monkinetic.blog/uploads/radio_userland.gif](https://monkinetic.blog/uploads/radio_userland.gif)

And my version in Goldfrog:

![https://monkinetic.blog/uploads/Screenshot 2024-04-11 at 11.39.49 AM.png](https://monkinetic.blog/uploads/Screenshot 2024-04-11 at 11.39.49 AM.png) 

- A small web app - not a static site generator
- Content stored ultimately as markdown files so they can be stored in git or similar
- Content indexed in sqlite for searching. serving various archive pages (tags, etc)

_UPDATE: As long as I'm dreaming, I wish it was easier to run a small web app like this off a container. I probably could with Digital Ocean's app platform, I haven't looked into it lately, and I'd still have to solve the "index in a sqlite db file" problem._
