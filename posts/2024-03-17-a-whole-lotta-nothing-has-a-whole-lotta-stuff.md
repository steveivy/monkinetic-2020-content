date: "2024-03-17T13:38:14Z"
slug: a-whole-lotta-nothing-has-a-whole-lotta-stuff
tags: ""
title: A Whole Lotta Nothing has a whole lotta stuff
---
Some time in the last couple of years I rediscovered [Matt Haughey's blog]. I subscribed to the RSS feed and am really enjoying his writing again. Bikes, electric vehicles, tech, and general stuff.

Matt's a great writer and has been doing it for my whole career.
