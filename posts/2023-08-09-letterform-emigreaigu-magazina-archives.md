date: "2023-08-09T13:01:46Z"
slug: letterform-emigreaigu-magazina-archives
tags: ""
title: 'Letterform: Emigre Magazina Archives'
---
Goodness. [Letterform Archive](https://letterformarchive.org/news/new-in-the-online-archive-complete-emigre-magazine/) has the [entire run of Emigré Magazine](https://oa.letterformarchive.org/?dims=Firms,Format&vals0=Emigre&vals1=Magazine) online.

![https://monkinetic.blog/uploads/emigre_3.png](https://monkinetic.blog/uploads/emigre_3.png)

Emigré was a huge influence on my graphic design education and short career in the early 90's, and I will be spending a lot of time in the archive in days to come.
