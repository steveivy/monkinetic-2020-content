date: "2023-08-04T01:46:08Z"
mastodon_id: "110828832957487178"
mastodon_url: https://toot.cafe/@sivy/110828832957487178
slug: no-elections-for-60-years
tags: voter-suppression,racism,voting-rights
title: 'Newbern, Alabama: No Elections for 60 years'
---

> I recently learned about a small town in Alabama that has not held a public election for more than sixty years. The town is Newbern, Alabama. While nearly eighty-five percent of the town residents are Black, before 2020 the town never had a Black mayor.

[An unbelievable and yet completely believable story of voter suppression in the deep south](https://www.ohfweekly.org/no-election-in-sixty-years/)

[Guy Nave, Jr](https://www.ohfweekly.org/author/guy/) gives a well-written and succint history of voting rights for former enslaved Black people in the south, then tells the story describing the incredible "hand-me-down" white mayorship in the majority-Black town.

> "We've never had an election out here. We don't have ballots and machines to do it." Stokes became mayor in 2008, when he inherited the position from Haywood Stokes Jr. Their ancestor, Peter P. Stokes, served in the Confederate Army and “owned” enslaved Black people when Newbern was a cotton plantation town.

