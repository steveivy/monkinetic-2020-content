date: "2023-08-02T14:26:50Z"
mastodon_id: "110820486503670491"
mastodon_url: https://toot.cafe/@sivy/110820486503670491
slug: novacolor-on-understanding-the-color-gray
tags: art,painting,arthistory
title: NovaColor on Understanding the Color Gray
---
And now for something different from NovaColor: [Understanding the Color Gray and Its Shades](https://novacolorpaint.com/blogs/nova-color/color-gray-and-its-shades)

I use NovaColor acrylics for [my artwork](https://sparktree.studio), and love their occasional articles on the theory and history of pigments. #art #painting #arthistory
