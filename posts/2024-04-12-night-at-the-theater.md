date: "2024-04-12T13:28:26Z"
slug: night-at-the-theater
tags: princessbride,theater,puppets
title: Night at the Theater
---
Went to the theater last night with my girlfriend(😁 👋), like a real adult. 

Except it was a shockingly hilarious parody puppet show version of The princess Bride (By S. Morgenstern) by the All Puppet Players, complete with alcohol, musical numbers, 4th wall breaking, flubs, ad-libs and improv. 

And I will never hear the lines "I'm going to do him left handed... if I use my right it's over too quickly!" the same again (Vizzini the puppet: "We didn't change those lines -- _at all_!!)

![https://monkinetic.blog/uploads/all_puppet_princess_bride.png](https://monkinetic.blog/uploads/all_puppet_princess_bride.png)
