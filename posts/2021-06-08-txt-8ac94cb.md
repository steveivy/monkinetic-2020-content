date: "2021-06-08T22:37:47Z"
slug: txt-8ac94cb
tags: ""
title: ""
twitter_id: "1402394499918090242"
twitter_url: https://twitter.com/steveivy/status/1402394499918090242
---
Recent Netflix binges:

- [Season 2 of Love Death and Robots](https://www.netflix.com/browse?jbv=80174608)
- [Jupiter's Legacy](https://www.netflix.com/browse?jbv=80244953)

Working on:

- [Ragnarok](https://www.netflix.com/browse?jbv=80232926)
