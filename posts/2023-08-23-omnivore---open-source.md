date: "2023-08-23T18:12:58Z"
slug: omnivore---open-source
tags: readlater,opensource
title: Omnivore - open source "read later" site and app
---
I was introduced to [Omnivore.app](https://omnivore.app/home) by someone on Mastodon (can't find the reference now, but thanks whoever you were)!

Omnivore is a "read later" app like Pocket, but free to use and [open source](https://github.com/omnivore-app/omnivore). I've found the website to be well done, though there are some issues right now:

- The iOS app freezes pretty often, so I do most of my reading on my Mac
- The [Firefox extension](https://omnivore.app/install/firefox) is easy to use but the "set labels" feature doesn't seem to work, and I general want to label/tag _everything_.
