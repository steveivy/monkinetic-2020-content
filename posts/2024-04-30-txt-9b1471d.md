date: "2024-04-30T15:43:46Z"
slug: txt-9b1471d
tags: ""
title: ""
---
Over on Mastodon `@obrhoff@chaos.social` [posted a wonderful variation/recreation of blueprints of the Star Trek: TOS tricorder](https://chaos.social/@obrhoff/112350590541110980). Go check them out!

[![https://monkinetic.blog/uploads/Screenshot 2024-04-30 at 8.43.17 AM.png](https://monkinetic.blog/uploads/Screenshot 2024-04-30 at 8.43.17 AM.png)](https://chaos.social/@obrhoff/112350590541110980)
