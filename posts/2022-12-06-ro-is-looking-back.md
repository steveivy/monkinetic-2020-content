date: "2022-12-06T21:32:41Z"
mastodon_id: "109468867974510681"
mastodon_url: https://toot.cafe/@sivy/109468867974510681
slug: ro-is-looking-back
tags: ""
title: Ro is looking back
---
[Are0h](https://roiskinda.cool/) is one of the main drivers behind a ground-breaking social media presence called Play Vicious (now defunct). Over on his blog he's telling the story of Play Vicious and the challenges he, MarciaX, and the site's members faced.

![https://monkinetic.blog/uploads/Screen Shot 2022-12-06 at 2.31.57 PM.png](https://monkinetic.blog/uploads/Screen Shot 2022-12-06 at 2.31.57 PM.png)

- [Recuerda PV : Chapter 1 - The Start, Part 1](https://roiskinda.cool/2022/11/recuerda-pv-chapter-1-the-start-part-1.html)
- [Recuerda PV : Chapter 2 - The Start, Part 2](https://roiskinda.cool/2022/12/recuerda-pv-chapter-2-the-start-part-2.html)

> I had long become accustomed to navigating primarily white and male spaces, the demographic over-represented in the tech industry, so I had a frame of reference for what to expect as I logged into my first Mastodon instance.

> Looking back, I can't say it was a terrible experience overall, but what surprised me was how concentrated the monoculture of white men of various tech skill levels present. It was _aggressively_ white.

I'm one of the cishet white dudes that populate the spaces that Ro is talking about. I've tried over the years to be pull in more and more voices from outside that bubble though, including some time where PV was one of my main social spaces. I didn't say much, I just tried to follow along and get a sense of what folks were talking about and where they were coming from -- and I count myself fortunate that Ro and MarciaX were always welcoming.

Since returning to the mastodon/fediverse (with Twitter's enflamé) I've been keeping my eyes out for how and where I can be a more effective advocate for making the fediverse a more safe space; still with a main goal to doing a lot of listening rather than speaking.
