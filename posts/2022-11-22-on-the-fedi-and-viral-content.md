date: "2022-11-22T15:58:24Z"
mastodon_id: "109388334680566956"
mastodon_url: https://toot.cafe/@sivy/109388334680566956
slug: on-the-fedi-and-viral-content
tags: twitter,twittermigration,fediverse,virality,viral,blacklivesmatter,metoo
title: On the Fedi and Viral Content
---
When people praise the lack of #viral content on Mastodon (or the #fediverse in general), it’s seems to be mostly white tech folks, happy for our clever bubbles to be left alone. 

But for people who desperately *need* to be seen and heard, going viral on Twitter is one of the only ways for their stories to get told. #BlackLivesMatter, oppression in the middle east, genocides in Rwanda and South Asia, the #metoo movement -- these movements couldn't be ignored because they grew fast and visibly, making it hard for them to be ignored, dismissed, or covered up.

The Fediverse as it exists right now would see these movements isolated, defederated, gated by content warnings, and probably DDOS’d by bad actors running malicious instances. ("Mal-odons"?) 

I guess right now I don’t want to see posts and think pieces about how "content can't go viral" on the Fediverse (whether or not it’s true) is *only a net-positive*. For all its faults Twitter has been a positive force for social change and visibility in millions of people’s lives.

We must learn from it and ask how — if we are going to make a case for the Fediverse as an alternative to Twitter — we can be better while not throwing those of us in the most need back to the wolves.

---

- This post was 93% inspired by [this excellent thread](https://twitter.com/shengokai/status/1594143042789085187?s=46&t=EPfnaVPcCystNvb8_toOGQ) by [@shengokai](https://twitter.com/shengokai) on #twitter.
- I got that link from [@jennyrae@wandering.shop](https://wandering.shop/@jennyrae/109373657688572337), thanks!
- This [meta-thread from @shengokai](https://twitter.com/shengokai/status/1590871165677240320) is also great reading on the #twittermigration

