date: "2023-08-09T03:50:27Z"
mastodon_id: "110857619262045290"
mastodon_url: https://toot.cafe/@sivy/110857619262045290
slug: art-from-an-ancient-future
tags: paintings
title: Art from an Ancient Future
---
[Love love love these "ancient future"](https://www.karlaknight.org/33-spaceships-for-another-planet-2017-19-/1) #paintings by Karla Night, and I am so there for Jason Kottke's description:

> Hilma af Klint as the production designer for Wes Anderson's Stargate

![https://monkinetic.blog/uploads/Screenshot 2023-08-08 at 8.47.13 PM.jpeg](https://monkinetic.blog/uploads/Screenshot 2023-08-08 at 8.47.13 PM.jpeg)

via [kottke](https://kottke.org/23/08/33-spaceships-for-another-planet)
