date: "2022-12-06T22:14:12Z"
slug: twitter-what-if-failure-is-the-plan
tags: twitter,failure,socialmedia
title: 'Twitter: What if failure is the plan?'
---
[Danah Boyd](https://zephoria.medium.com/): [What if failure is the plan?](https://zephoria.medium.com/what-if-failure-is-the-plan-2f219ea1cd62)

> Network effects intersect with perception to drive a sense of a site’s social relevance and interpersonal significance.

#twitter #failure #socialmedia
