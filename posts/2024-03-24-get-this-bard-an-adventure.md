date: "2024-03-24T21:23:08Z"
slug: get-this-bard-an-adventure
tags: ttrpg,roleplay,dnd
title: 'Get this Bard an adventure '
---
Posted to our #ttrpg group text:

"Curran is 6 days into a drunken binge after being carried kitten-style by that shape-shifted tiger.

He's been kicked out of 4 taverns for beating other musicians with what was described as "a fucking oboe, and it hurt!" while singing scraps of drinking songs, and two more for storming the stage, yelling "wrong lyrics, you tone-deaf bag of air!"

This man needs another adventure, soon"

(Curran is my human bard :D ) #roleplay #dnd
