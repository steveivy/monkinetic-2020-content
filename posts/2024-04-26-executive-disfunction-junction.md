date: "2024-04-26T20:09:21Z"
slug: executive-disfunction-junction
tags: adhd,neurodivergence,selfreporting,psychology
title: Executive Disfunction Junction
---
Was pointed to this standard test of executive function (or at least one's experience of it) called the [ESQ-R](https://embrace-autism.com/executive-skills-questionnaire-revised/), the "Executive Skills Questionnaire, Revised". Always one to see what these self-reporting tools say about by neurodivergence, I [took the test here](https://www.smartbutscatteredkids.com/esq/).

No idea as to the test's ultimate validity but this tracks:

![https://monkinetic.blog/uploads/esq-r.png](https://monkinetic.blog/uploads/esq-r.png)
