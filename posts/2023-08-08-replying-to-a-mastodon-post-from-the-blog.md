date: "2023-08-08T13:50:26Z"
slug: replying-to-a-mastodon-post-from-the-blog
tags: mastodonapi,fediverse,programming,blogging,indieweb
title: Replying to a Mastodon post from the blog
---
Fedi/Mastodon programmers... with the #MastodonAPI, and given a url to a post on any instance (assuming I have access to the toot from my account), how might I get *my* instance to fetch it and give me a "local" ID that is suitable for passing as the "inReplyToID" in a toot payload?

Wondering if I need to:

- perform a search (https://docs.joinmastodon.org/methods/search/)
- find the relevant status in the results
- use the ID for the status

Would that be the "local" ID?

#MastodonAPI #fediverse #programming #blogging #indieweb
