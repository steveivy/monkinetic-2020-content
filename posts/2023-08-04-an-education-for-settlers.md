date: "2023-08-04T23:45:13Z"
mastodon_id: "110834029437915497"
mastodon_url: https://toot.cafe/@sivy/110834029437915497
slug: an-education-for-settlers
tags: colonialism,settlercolonialism,whitesupremacy,education
title: An Education for Settlers on Indigenous Peoples Day
---
[Yehuda Rothschild](https://yehudarothschild.com/) republished (with permission) [a presentation](https://yehudarothschild.com/introduction-to-settler-colonialism/) by Lara A Jacobs, Mvskoke citizen and [Native scientist](https://www.researchgate.net/profile/Lara-Jacobs-2), on settler colonialism - what is it, what is colonization, what happened when settlers (in North America) arrived.

**The whole thing is excellent, and I hope there's a video version, or one is done one day.**

Some particular points I want to mention/synthesize:

Colonization is not a historical event, but a continuous process that requires ongoing support through social and legal systems and institutional violence.

Colonization doesn't just mean moving in and kicking out indiginous people, though it definitely means that. It's also continued occupation of their "lands, waters, and environments", the forced breakup of families and communities through unjust and cruel laws and foster systems, the intentional obliteration of native peoples' _languages_, _cultural practices_, and _value systems_ (often through forced fostering and the original residential homes).

We (the White European Settlers) operated -- and still operate -- under a value system and ideologies that are manifestly destructive (the following is quoted):

> - Conquer and defeat their surroundings
> - Biases against undeveloped areas
> - Associated uncivilized areas with evil
> - Commodity-based utilitarianism (e.g., extractive practices)
> - Extraction of 'natural resources' (e.g., forest products, marine fisheries, mining, etc.)
> - Capitalism

All of these in direct opposition to the beliefs, ideologies, and practices of Native cultures and communities, which had been existing largely in balance with their environments for many thousands of years before settlers arrived.

There is much much more in the slides that were shared and I'm grateful to Lara and Yehuda both for making these available.
