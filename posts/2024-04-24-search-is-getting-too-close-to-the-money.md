date: "2024-04-24T18:44:51Z"
slug: search-is-getting-too-close-to-the-money
tags: ""
title: '"Search is getting too close to the money"'
---
![https://monkinetic.blog/uploads/Screenshot 2024-04-24 at 11.44.35 AM.png](https://monkinetic.blog/uploads/Screenshot 2024-04-24 at 11.44.35 AM.png)

[Ed Zitron](https://www.wheresyoured.at/) is an extraordinary writer, who I just discovered via (I have no idea which app, email, or newsletter) but here are a couple of his stories to make your internet-loving heart burn with rage:

- [The Man Who Killed Google Search](https://www.wheresyoured.at/the-men-who-killed-google/) -- I can remember the cramped shared office I was in when I first tried Google to search the internet and my mind was blown. Ed's story is heartbreaking. -- Steve
- [They're Looting the Internet](https://www.wheresyoured.at/the-great-looting-of-the-internet/) -- quote from the previous link but fits here: "search is getting too close to the money"
