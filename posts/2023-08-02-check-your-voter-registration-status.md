date: "2023-08-02T18:39:35Z"
mastodon_id: "110821479255596216"
mastodon_url: https://toot.cafe/@sivy/110821479255596216
slug: check-your-voter-registration-status
tags: ""
title: Check your Voter Registration Status
---
From the [Black Voters Matter Fund](https://blackvotersmatterfund.org/) mailing list (I can't find a web link for this on the website):

> Georgia Voters, Your Voter Status Is At Risk

> Hundreds of thousands of Georgians could see their registrations canceled, causing concerns among voting rights advocates. The Georgia Secretary of State plans to remove 191,473 inactive voters, citing outdated lists. While accuracy is essential, this will disenfranchise voters in the process. Protect your right to vote - check your status today at https://mvp.sos.ga.gov, and stay informed about these changes. Let your voice be heard in Georgia's elections!


It's a good reminder for all of us - especially anyone who believes in actual Democracy, justice, and law - that that the GOP's "best" chance these days is to repeatedly attack our voting rights, most particularly those of oppressed "minorities".

("People of the global majority" I saw someone brilliantly describe those we often call "people of color".)
