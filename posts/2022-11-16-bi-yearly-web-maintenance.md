date: "2022-11-16T18:27:52Z"
slug: bi-yearly-web-maintenance
tags: blogging,socialmedia,federation,mastodon,goldfrog,devops
title: Time for bi-yearly web presence maintenance
twitter_id: "1592966481049923584"
twitter_url: https://twitter.com/steveivy/status/1592966481049923584
---
What with Twitter (aka birdsite, hellsite, muskosite) flailing in the clammy hands of Dr. No, and interest in the [federated web](https://monkinetic.blog/2017/12/11/the-revolution-will-be-federated) re-emerging, I figured it was time to review my own web presence and see what was the situation.

![https://monkinetic.blog/uploads/this_is_fine.jpg](https://monkinetic.blog/uploads/this_is_fine.jpg)

**Dear reader, it was Not Good.**

Warning one was hitting this site from my work network and getting a BitDefender screen of doom saying the site was serving a keylogger. *NOT GOOD*.

Then the site - which was hosted on [Linode](https://www.linode.com/) and runs my own homegrown blog software, [Goldfrog](https://gitlab.com/steveivy/goldfrog) - went completely down. After some "where did those ssh keys get to, where is this thing anyway" I got logged in and figured out that my server had been hacked in some way, TLS and letsencrypt removed. I haven't had time to troll the logs for evidence as to how the server was accessed, but I downloaded them and have them set aside to look later.

## We Can Rebuild It

Thus entered a week of figuring out once again how the heck [Monkinetic](https://monkinetic.blog) is built and deployed, migrating the code from Github to Gitlab (which I'm more familiar with due to $dayjob), and refactoring the Ansible code that builds the server and deploys the blog/content.

Finally today I got it 85% done, which is pretty good for a full migration between hosting providers (I also moved from Linode to [Digital Ocean](https://www.digitalocean.com/) where I already have some other services).

## Masto-tootly-don

With the insanity on Twitter, I logged back into my [Mastodon account on toot.cafe](https://toot.cafe/@sivy) and enjoyed the huge stream of new folks migrating from Twitter to federated platforms (mostly to [mastodon.social](https://mastodon.social/explore) since that's the first/largest instance, but folks are making their way from there to smaller instances as they get more comfortable).

Apparently Mastodon 4.0 is out (release candidate) and they've changed the annoying-until-it-was-gone "Toot" to ["Publish"](https://github.com/mastodon/mastodon/pull/18583). I'd have preferred "Post" myself, but 🤷‍♀️.

[Maybe servers should just change it to suit their audience?](https://toot.cafe/@sivy/109351296886897078)
